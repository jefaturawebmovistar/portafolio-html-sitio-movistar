Vue.directive('focus', {

    // Cuando el elemento enlazado es insertado en el DOM

    inserted: function (el) {
        console.log("llegué aquí");
        // Pon el foco en el elemento
        el.focus();
        console.log("llegué aquí también");
    }
})
var appVue = new Vue({
    el: '#contenedor_vue',

    data: {

        planSeleccionado: "",

        "paquetes": {

            "paquete-1": {
                "nombre": "Paquete Lite",
                "posicionMovil": 1,
                "beneficioDestacado1": "TV en vivo",
                "beneficioDestacado2": "y más de 5000 títulos gratis",
                "destacadoBorde": "borde-top-azul-5",
                "destacadoBoton": "btn-azul-movistar",
                "destacadoIcono": "pt-4",
                "beneficios": {
                    "ben1": {
                        "class": "bold",
                        "text": "Películas y series exclusivas de Movistar",
                    },
                    "ben2": {
                        "text": "Movistar Series"
                    },
                    "ben3": {
                        "text": "Películas, documentales, infantiles."
                    },
                    "ben4": {
                        "class": "bold pt-2",
                        "text": "Canales en vivo"
                    }

                },
                "canales": {
                    "canal1": {
                        "icon": "icon-icono-facebook-filled",
                    },
                    "canal2": {
                        "icon": "icon-icono-facebook-filled"
                    },
                    "canal3": {
                        "icon": "icon-icono-facebook-filled"
                    },
                    "canal4": {
                        "icon": "icon-icono-facebook-filled"
                    },
                    "canal5": {
                        "icon": "icon-icono-facebook-filled"
                    }

                },
                "textoBoton": "Registrarse"
            },
            "paquete-2": {
                "nombre": "Paquete Full",
                "posicionMovil": 2,
                "beneficioDestacado1": "33 canales",
                "beneficioDestacado2": "Internacionales",
                "destacadoBorde": "borde-top-verde-5",
                "destacadoBoton": "btn-verde-movistar",
                "destacadoIcono": "icon-icono-recarga-movil-filled icon-verde-movistar",
                "canales": {
                    "canal1": {
                        "icon": "icon-icono-facebook-filled",
                    },
                    "canal2": {
                        "icon": "icon-icono-facebook-filled"
                    },
                    "canal3": {
                        "icon": "icon-icono-facebook-filled"
                    },
                    "canal4": {
                        "icon": "icon-icono-facebook-filled"
                    },
                    "canal5": {
                        "icon": "icon-icono-facebook-filled"
                    },
                    "canal6": {
                        "icon": "icon-icono-facebook-filled",
                    },
                    "canal7": {
                        "icon": "icon-icono-facebook-filled"
                    },
                    "canal8": {
                        "icon": "icon-icono-facebook-filled"
                    },
                    "canal9": {
                        "icon": "icon-icono-facebook-filled"
                    },
                    "canal10": {
                        "icon": "icon-icono-facebook-filled"
                    },
                    "canal11": {
                        "icon": "icon-icono-facebook-filled",
                    },
                    "canal12": {
                        "icon": "icon-icono-facebook-filled"
                    },
                    "canal13": {
                        "icon": "icon-icono-facebook-filled"
                    },
                    "canal14": {
                        "icon": "icon-icono-facebook-filled"
                    },
                    "canal15": {
                        "icon": "icon-icono-facebook-filled"
                    }
                },
                "precio": 25.50,
                "textoBoton": "Lo quiero"
            },
        },
        "paquetesExtra": {
            "paquete-1": {
                "nombre": "FOX PREMIUM",
                "posicionMovil": 1,
                "color": "bg-naranja-movistar text-blanco",
                "canales": {
                    "canal1": {
                        "icon": "icon-icono-facebook-filled",
                    },
                    "canal2": {
                        "icon": "icon-icono-facebook-filled"
                    },
                    "canal3": {
                        "icon": "icon-icono-facebook-filled"
                    },
                    "canal4": {
                        "icon": "icon-icono-facebook-filled"
                    },
                    "canal5": {
                        "icon": "icon-icono-facebook-filled"
                    }
                },
                "precio": 9.90,
                "textoBoton": "Contratar"
            },
            "paquete-2": {
                "nombre": "HBO GO",
                "posicionMovil": 2,
                "color": "bg-gris-movistar-5 text-blanco",
                "canales": {
                    "canal1": {
                        "icon": "icon-icono-facebook-filled",
                    },
                    "canal2": {
                        "icon": "icon-icono-facebook-filled"
                    },
                    "canal3": {
                        "icon": "icon-icono-facebook-filled"
                    },
                    "canal4": {
                        "icon": "icon-icono-facebook-filled"
                    },
                    "canal5": {
                        "icon": "icon-icono-facebook-filled"
                    }
                },
                "precio": 9.90,
                "textoBoton": "Contratar"
            }
        },
        "peliculas": {
            "pelicula-1": {
                "nombre": "Nombre de Película",
                "imagen": "https://imagessl8.casadellibro.com/a/l/t0/38/9788416548538.jpg",
                "tag": "PREMIUM",
                "colorTag": "bg-naranja-movistar",
                "precio": 0.88,
                "textoEnlace": "Alquilar"
            },
            "pelicula-2": {
                "nombre": "Nombre de Película",
                "imagen": "https://imagessl8.casadellibro.com/a/l/t0/38/9788416548538.jpg",
                "tag": "LITE",
                "colorTag": "bg-azul-movistar",
                "precio": 0.88,
                "textoEnlace": "Alquilar"
            },
            "pelicula-3": {
                "nombre": "Nombre de Película",
                "imagen": "https://imagessl8.casadellibro.com/a/l/t0/38/9788416548538.jpg",
                "tag": "LITE",
                "colorTag": "bg-azul-movistar",
                "precio": 0.88,
                "textoEnlace": "Alquilar"
            },
            "pelicula-4": {
                "nombre": "Nombre de Película",
                "imagen": "https://imagessl8.casadellibro.com/a/l/t0/38/9788416548538.jpg",
                "tag": "LITE",
                "colorTag": "bg-azul-movistar",
                "precio": 10.30,
                "textoEnlace": "Alquilar"
            }

        }

    },
    methods: {
        cambiarFlecha: function (id) {
            $('#' + id).toggleClass("transform-180");
        },
        cambiarTexto: function (id, texto) {
            var textoObtenido = $('#' + texto).text()
            textoObtenido = textoObtenido.replace("\n", "");
            textoObtenido = textoObtenido.replace(/ /g, "");
      
      
            if (textoObtenido == "Másdetalle") {
              $('#' + texto).text("Menos detalle")
              $('#' + id).addClass("transform-180");
            } else if (textoObtenido == "Menosdetalle") {
              $('#' + texto).text("Más detalle")
              $('#' + id).removeClass("transform-180");
            }
          },
    },
})


