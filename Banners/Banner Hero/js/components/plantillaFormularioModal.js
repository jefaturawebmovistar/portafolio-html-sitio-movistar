
var componenteplantillaFormularioModal = Vue.component("formulario-modal", function (resolve, reject) {
  resolve({
    template: '<div class="modal fade" id="bannerHeroFormulario" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">' +
      '<div class="modal-dialog" role="document">' +
      '<div class="modal-content">' +
      '<div class="modal-header">' +
      '<button type="button" class="close" data-dismiss="modal" aria-label="Close">' +
      '<span aria-hidden="true" class="font-size-150 mr-3 icon-close text-gris-movistar-5">&times;</span>' +
      '</button>' +
      '</div>' +
      '<div class="modal-body">' +
      '<h3 class="text-center light">Te ayudamos a contratar tu<br/> Plan Movistar</h3>' +
      '<form class="mt-5">' +
      '<div class="row">' +
      '<div class="form-group col-12">' +
      '<label for="input-text-celular">Número celular <span class="text-magenta-movistar">*</span></label>' +
      '<input type="text" class="form-control" :class="isValidNumero" v-model="numeroCelular" @blur="validarNumero" @input="activarBoton" id="input-text-celular">' +
      '</div>' +
      '</div>' +
      '<div class="row">' +
      '<div class="form-group col-12">' +
      '<label for="input-text-cedula">Número cédula <span class="text-magenta-movistar">*</span></label>' +
      '<input type="text" class="form-control" :class="isValidCedula" v-model="numeroCedula" @blur="validarCedula" @input="activarBoton" id="input-text-cedula">' +
      '</div>' +
      '</div>' +
      '<div class="row mt-3">' +
      '<div class="form-group col-12">' +
      '<button type="button" class="btn btn-verde-movistar btn-block button-enviar" :disabled="botonEnviar == true" @click="enviarDatos"> Enviar mis Datos</button>' +
      '</div>' +
      '</div>' +
      '</form>' +
      '</div>' +
      '</div>' +
      '</div>' +
      '</div>',
    data: function () {
      return {
        numeroCelular: "",
        numeroCedula: "",
        botonEnviar: false,
        urlEnvioDatos: "https://www.rigel-m.com/EnviarDatosFormulario",
        datosEnvio: "",
        isValidNumero: "",
        isValidCedula: ""
      }
    },
    methods: {
      enviarDatos: function () {
        console.log(this.numeroCelular);
        console.log(this.numeroCedula);
        $('.modal-body').html('<div class="cargando" style="margin-top: 70%; left: 40%;"> <div></div> <div></div> <div></div> <div></div> </div>');
        axios.post('https://www2.movistar.com.ec/send-obo-site-new/send/datos', {
          cellphone: this.numeroCelular,
          identification: this.numeroCedula,
          campana: "es una prueba",
          producto: "ALTA"
        })
          .then(function (response) {
            console.log(response);
            $('.modal-body').html(response.data.smsObo);
            $('.modal-body').css({"padding-top":"50%"});
            setTimeout(function () {
              $('#bannerHeroFormulario').modal('hide');
              $('.modal-body').html('<h3 class="text-center light">Te ayudamos a contratar tu<br> Plan Movistar</h3><form class="mt-5"><div class="row"><div class="form-group col-12"><label for="input-text-celular">Número celular <span class="text-magenta-movistar">*</span></label><input type="text" id="input-text-celular" class="form-control"></div></div><div class="row"><div class="form-group col-12"><label for="input-text-cedula">Número cédula <span class="text-magenta-movistar">*</span></label><input type="text" id="input-text-cedula" class="form-control"></div></div><div class="row mt-3"><div class="form-group col-12"><button type="button" class="btn btn-verde-movistar btn-block button-enviar"> Enviar mis Datos</button></div></div></form>');
              this.numeroCelular = "";
              this.numeroCedula = "";
            }, 4000);
          })
          .catch(function (error) {
            console.log(error);
            console.log("aquí tampoco soy el error");
          });
      },
      activarBoton: function () {
        if (this.numeroCelular.length == 10 && this.numeroCedula.length == 10) {
          this.botonEnviar = false;
        }
      },
      validarNumero: function () {
        this.isValidNumero = this.numeroCelular.length == 10 ? 'is-valid' : 'error';
      }
      ,
      validarCedula: function () {
        this.isValidCedula = this.numeroCedula.length == 10 ? 'is-valid' : 'error';
      },
    },
    mounted: function () {
    }
  });
  // });
});



