var componenteplantillaFormularioWeb = Vue.component("formulario-web", function (resolve, reject) {
  resolve({
    template: '<div class="">' +
      '<form>' +
      '<div class="row form-row">' +
      '<div class="form-group col-3">' +
      '<input type="number" class="form-control text-gris-movistar-5 input-celular" :class="isValidNumero" v-model="numeroCelular" @blur="validarNumero" @input="activarBoton" placeholder="Número celular">' +
      '</div>' +
      '<div class="form-group col-3">' +
      '<input type="number" class="form-control text-gris-movistar-5 input-cedula" :class="isValidCedula" v-model="numeroCedula" @blur="validarCedula" @input="activarBoton" placeholder="Número cédula">' +
      '</div>' +
      '<div class="form-group col-2">' +
      '<button type="button" class="btn btn-verde-movistar button-enviar" :disabled="botonEnviar == true" @click="enviarDatos">Enviar Datos</button>' +
      '</div>' +
      '</div>' +
      '</form>' +
      '</div>',
    data: function () {
      return {
        numeroCelular: "",
        numeroCedula: "",
        botonEnviar: true,
        urlEnvioDatos: "https://www2.movistar.com.ec/send-obo-site-new/send/datos",
        datosEnvio: "",
        isValidNumero: "",
        isValidCedula: ""
      }
    },
    methods: {
      enviarDatos: function () {
        console.log(this.numeroCelular);
        console.log(this.numeroCedula);

        $('.button-enviar').html('<div class="cargando desktop cargando-small"> <div></div> <div></div> <div></div> <div></div> </div>');

        axios.post('https://www2.movistar.com.ec/send-obo-site-new/send/datos', {
          cellphone: this.numeroCelular,
          identification: this.numeroCedula,
          campana: "es una prueba",
          producto: "ALTA"
        })
          .then(function (response) {
            console.log(response);
            $('#exampleModal').addClass('modal-success-desktop');
            $('#exampleModal').find('.modal-body').html(response.data.smsObo);
            $('#exampleModal').find('.modal-body').find('.text-center').removeClass('col-md-4');
            $('#exampleModal').find('.modal-body').find('.text-center').addClass('col-md-10');
            $('#exampleModal').modal('show');
            setTimeout(function () {
              $('#exampleModal').modal('hide');
              $('.button-enviar').html('Enviar Datos');
              this.numeroCelular = "";
              this.numeroCedula = "";
            }, 4000);
          })
          .catch(function (error) {
            console.log(error);
            console.log("aquí tampoco soy el error");
          });
      },
      activarBoton: function () {
        if (this.numeroCelular.length == 10 && this.numeroCedula.length == 10) {
          this.botonEnviar = false;
        }
      },
      validarNumero: function () {
        this.isValidNumero = this.numeroCelular.length == 10 ? 'is-valid' : 'error';
      },
      validarCedula: function () {
        this.isValidCedula = this.numeroCedula.length == 10 ? 'is-valid' : 'error';
      },
    },
    mounted: function () {
    }
  });
});



