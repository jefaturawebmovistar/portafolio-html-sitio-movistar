var rutas = [
    {
        path: "/plantillaFormularioWeb",
        name: "plantillaFormularioWeb",
        component: componenteplantillaFormularioWeb,
        props: (route) => ({ datos: route.params })
    },
    {
        path: "/plantillaFormularioModal",
        name: "plantillaFormularioModal",
        component: componenteplantillaFormularioModal,
        props: (route) => ({ datos: route.params })
    },
];

const router = new VueRouter({
    routes: rutas
});