// window.onload = function () {
var appVue = new Vue({
    el: '#contenedor_vue',
    // router: router,
    data: {
        celular: "",
        cedula: "",
        mostrarCargando: false,
        mostrarMensajeSatisfactorio: false,
    },
    methods: {
        activarCargando: function (datoBoolean) {
            this.mostrarRouter = datoBoolean ? false : true;
            this.mostrarCargando = datoBoolean;
        },
        mensajeSatisfactorio: function (datoBoolean) {
            this.mostrarRouter = datoBoolean ? false : true;
            this.mostrarMensajeSatisfactorio = datoBoolean;
        },

    },
    mounted: function () {

        this.activarCargando(true);

    },
    components: {
        componenteplantillaFormularioWeb: componenteplantillaFormularioWeb,
        componenteplantillaFormularioModal: componenteplantillaFormularioModal
    }


});




// }