//NOTA: las directivas y sus validaciones deben estar en otro javascript y agregar el Script al HTML

Vue.directive('celular', {
  update: function (el, binding, vnode) {

    const appVue = vnode.context; // nombre de la instancia de vueJS, en este caso se llama appVue;
    const nombreDirectiva = binding.name; // en este caso se llama celular igual que el el dato: input.celular IMPORTANTE, y si no se desea hacerlo así se puede cambiar la lógica si ningún problema
    let input = appVue.input[nombreDirectiva];// el data en el appVue contiene una propiedad llamada input, es un objeto que tiene los valores del input
    let valor = binding.value; // en el binding viene el dato actualizado en el input

    let validacionCelular = validarCelular(valor); // realiza la validación del celular

    input.valor = validacionCelular.valor; // actualiza el valor del input.celular
    input.muestraMensaje = validacionCelular.muestraMensaje; // es un valor booleano utilizado para presentar el mensaje
    input.mensaje = validacionCelular.mensaje; // muestra el mensaje respectivo de la validación del celular1
    input.error = validacionCelular.error; // es un booleano para presentar el error
  }
});

// falta agregar la validación de la cédula y agregar la directiva en el HTML, revisar cómo está el del celular y 
// adecuarlo como se necesite
Vue.directive('cedula', {

  update: function (el, binding, vnode) {
    const appVue = vnode.context;
    const nombreDirectiva = binding.name; // en este caso se llama celular igual que el el dato: input.celular IMPORTANTE, y si no se desea hacerlo así se puede cambiar la lógica si ningún problema
    let input = appVue.input[nombreDirectiva];
    let valor = binding.value;

    let validacionCedula = validarCedula(valor); // ESTE MÉTODO NO EXISTE, POR FAVOR AGREGAR EL CÓDIGO RESPECTIVO PARA TENER LA VALIDACIÓN

    input.valor = validacionCedula.valor;
    input.muestraMensaje = validacionCedula.muestraMensaje;
    input.mensaje = validacionCedula.mensaje;
    input.error = validacionCedula.error;
  }
});
function validarCedula(valor) {
  let valorDeRetorno = {
    valor: valor,
    mensaje: '',
    muestraMensaje: false,
    error: false
  };

  let telf_completo = valor ? valor : "";
  let telf = telf_completo.split(""); // separa los dígitos en un arreglo de array con los números ingresados para verificar la validación del celular por dígito
  if (telf.length <= 0) {
    valorDeRetorno.mensaje = '',
      valorDeRetorno.muestraMensaje = false;
    valorDeRetorno.valor = telf_completo;
  } else if (telf.length < 10) {
    valorDeRetorno.mensaje = telf.length >= 10 ? "Has ingresado un número correcto" : 'Estás ingresando un número de cédula';
    valorDeRetorno.error = true;
    valorDeRetorno.muestraMensaje = telf.length < 10 ? true : false;
  } else if (telf.length > 10) { // si tiene más de 10 elimina el último dígito ingresado para que dar con un teléfono válido
    valorDeRetorno.valor = telf_completo.substr(0, 10);
    valorDeRetorno.muestraMensaje = false;
    valorDeRetorno.error = true;
  } else if (telf.length == 10) { // si el número es igual a 10 la validación está correcta
    var total = 0;
    var longitud = telf.length;
    var longcheck = longitud - 1;

    for (i = 0; i < longcheck; i++) {
      if (i % 2 === 0) {
        var aux = valor.charAt(i) * 2;
        if (aux > 9) aux -= 9;
        total += aux;
      } else {
        total += parseInt(valor.charAt(i)); // parseInt o concatenará en lugar de sumar
      }
    }

    total = total % 10 ? 10 - total % 10 : 0;

    if (valor.charAt(longitud - 1) == total) {
      valorDeRetorno.error = false;
      valorDeRetorno.valor = telf_completo;
      valorDeRetorno.muestraMensaje = false;

    } else {

      valorDeRetorno.error = true;
      valorDeRetorno.valor = telf_completo;
      valorDeRetorno.muestraMensaje = true;
    }




  }


  return valorDeRetorno;
};

function validarCelular(valor) {

  let valorDeRetorno = {
    valor: valor,
    mensaje: '',
    muestraMensaje: false,
    error: false
  };

  let telf_completo = valor ? valor : "";
  let telf = telf_completo.split(""); // separa los dígitos en un arreglo de array con los números ingresados para verificar la validación del celular por dígito
  if (telf.length <= 0) {
    valorDeRetorno.mensaje = '',
      valorDeRetorno.muestraMensaje = false;
    valorDeRetorno.valor = telf_completo;
  } else if (telf[0] != 0) { // si el 1er dígito no es cero retornar mensaje;
    return {
      muestraMensaje: true,
      mensaje: "Número de celular inválido",
      error: true,
      valor: ''
    };
  } else if (telf[0] == 0 && telf.length > 1) { // si el 1er dígito es 0 y ya contiene más números hacer =>
    if (telf[1] != 9) { // si el 2do número no es 9 retornar mensaje
      return {
        muestraMensaje: true,
        mensaje: "Número de celular inválido",
        error: true,
        valor: telf_completo.substr(0, 1) // al teléfono original elimina el número que no es 9
      };
    } else if (telf[0] == 0 && telf[1] == 9) { // si el 1er dígito es 0 y el 2do es 9, entonces se está ingresado un número correcto
      valorDeRetorno.mensaje = telf.length >= 10 ? "Has ingresado un número correcto" : 'Estás ingresando un número celular';
      valorDeRetorno.error = true;
      valorDeRetorno.muestraMensaje = telf.length < 10 ? true : false;
      if (telf.length > 10) { // si tiene más de 10 elimina el último dígito ingresado para que dar con un teléfono válido
        valorDeRetorno.valor = telf_completo.substr(0, 10);
        valorDeRetorno.muestraMensaje = false;
        valorDeRetorno.error = true;
      } else if (telf.length == 10) { // si el número es igual a 10 la validación está correcta
        valorDeRetorno.valor = telf_completo;
        valorDeRetorno.muestraMensaje = false;
        valorDeRetorno.error = false;
      }
    }
  }
  return valorDeRetorno;
};

Vue.directive('codigo', {

  update: function (el, binding, vnode) {

    console.log('ingreso direcgfdstiva');
    console.log('ingreso directiva');

    const appVue = vnode.context;
    const nombreDirectiva = binding.name;
    let input = appVue.input[nombreDirectiva];
    let valor = binding.value.valor;
    let errorServicio = binding.value.errorServicio;

    if (errorServicio) {
      console.log('error servicio');
      input.valor = valor;
      input.muestraMensaje = input.muestraMensaje;
      input.mensaje = input.mensaje;
      input.error = input.error;
      input.errorServicio = false;
    } else {
      let validacionCodigo = validarCodigo(valor);
      input.valor = validacionCodigo.valor;
      input.muestraMensaje = validacionCodigo.muestraMensaje;
      input.mensaje = validacionCodigo.mensaje;
      input.error = validacionCodigo.error;
    }


  }
});
function validarCodigo(valor) {

  let valorDeRetorno = {
    valor: valor,
    mensaje: '',
    muestraMensaje: false,
    error: false
  };

  let telf = valor ? valor : "";
  if (telf.length <= 0) {
    valorDeRetorno.mensaje = '',
      valorDeRetorno.muestraMensaje = false;
    valorDeRetorno.valor = telf;

  } if (telf.length < 4 && telf.length != '') {
    valorDeRetorno.mensaje = telf.length < 4 ? 'El código debe contener 4 dígitos' : '';
    valorDeRetorno.valor = telf.substr(0, 4);
    valorDeRetorno.muestraMensaje = true;
    valorDeRetorno.error = true;
  } else if (telf.length > 4) {
    valorDeRetorno.valor = telf.substr(0, 4);
    valorDeRetorno.muestraMensaje = false;
    valorDeRetorno.error = true;
  } else if (telf.length == 4) {
    valorDeRetorno.valor = telf;
    valorDeRetorno.muestraMensaje = false;
    valorDeRetorno.error = false;
  }


  return valorDeRetorno;
};