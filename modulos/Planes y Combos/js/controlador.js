
var appVue = new Vue({
  el: '#contenedor_vue_escaleras',

  data: {
    accion: "",
    /* DIRECTIVAS */
    input: {
      celular: { valor: "0", error: true, muestraMensaje: false, mensaje: '' },
      cedula: { valor: "0", error: null, muestraMensaje: false, mensaje: '' }, // falta la implementación para la validación de la cédula
      codigo: { valor: "0", error: null, muestraMensaje: false, mensaje: '', errorServicio: false }
    },
    cantidadTelefonos:"",
    respuesta: false,
    datosPlanes: [],
    datosCombos: [],
    datosCelulares:[],
    precioSeleccionado: {},
    gigasSeleccionado: {},
    estilosPlanDestacado: {
      0: {
        "destacadoBorde": "borde-top-azul-5",
        "destacadoBoton": "btn-azul-movistar",
        "destacadoIcono": "",
      },
      1: {
        "destacadoBorde": "borde-top-verde-5",
        "destacadoBoton": "btn-verde-movistar",
        "destacadoIcono": "icon-icono-recarga-movil-filled icon-verde-movistar",

      }
    },

    planSeleccionado: "",
  },
  computed: {
    abrir: function () {
      if (this.input.celular.valor.split('').length < 10) {

        return true
      } else if (this.input.celular.error == true) {
        return true
      }

    },
    activoBoton2: function () {
      if (this.input.codigo.valor.split('').length < 4) {
        return true;
      } else if (this.input.codigo.error == true) {
        return true;
      }

    },
    habilitarBotonPlanes: function () {
      if (this.input.celular.valor.split('').length < 10 || this.input.cedula.valor.split('').length < 10) {
        return true
      } else if (this.input.celular.error == true || this.input.cedula.error == true) {
        return true
      } else {
        return false
      }

    }
  },


  created() {

    window.addEventListener('resize', this.ejecutarEvento)
    this.cargarData();

  },
  destroyed() {
    window.removeEventListener('resize', this.ejecutarEvento)
  },


  methods: {
    ejecutarEvento: function () {
      this.datosPlanes = this.handleResize(this.datosPlanes);
      this.datosCombos = this.handleResize(this.datosCombos);

    },

    cargarData: function () {
      let identificadorCatalogo = "catalogo-planes";
      /* axios.post("json/plans_largue.json", {
        identificador: identificadorCatalogo
        /documents/328836/467193/plans_largue.json
      }) */
      axios.get("json/plans_largue.json")
        .then(respuesta => {

          this.datosPlanes = respuesta.data[identificadorCatalogo].plans;
          this.datosCombos = respuesta.data[identificadorCatalogo].combos;
          this.datosCelulares = respuesta.data[identificadorCatalogo].cellphones;
          this.cantidadTelefonos =this.datosCelulares.length
          console.log(this.cantidadTelefonos,"soy la cantidad telefonos")     


          for (const key in this.datosPlanes) {
            if (this.datosPlanes.hasOwnProperty(key)) {
              const element = this.datosPlanes[key];
            }
          }

          this.datosPlanes = this.handleResize(this.datosPlanes);
          this.datosCombos = this.handleResize(this.datosCombos);

        });

    },
    ordenar: function (datos, key) {
      datos = Object.values(datos);
      datos.sort(function (a, b) {
        return a[key] - b[key];
      });
      return datos;
    },
    cambiarTexto: function (id, texto) {
   
      var textoObtenido = $('#' + texto).text()
      textoObtenido = textoObtenido.replace("\n", "");
      textoObtenido = textoObtenido.replace(/ /g, "");

      if (textoObtenido == "Másdetalle") {
        $('#' + texto).text("Menos detalle");
        $('#' + id).addClass("transform-180");
      } else if (textoObtenido == "Menosdetalle") {
        $('#' + texto).text("Más detalle");
        $('#' + id).removeClass("transform-180");
      }
    },
    abrirModal: function (slug) {

      this.planSeleccionado = this.datosPlanes.find(variable => variable.slug == slug);

      this.gigasSeleccionado = this.planSeleccionado.normal_benefits[0];
      this.precioSeleccionado = this.planSeleccionado.prices.price;


      this.input.celular.valor = ""
      this.input.cedula.valor = ""


      $('#exampleModalCenter').modal('toggle');
    },
    abrirModalCombos: function (slug) {

      this.planSeleccionado = this.datosCombos.find(variable => variable.slug == slug);
      this.gigasSeleccionado = this.planSeleccionado.gigas;
      this.precioSeleccionado = this.planSeleccionado.price;

      this.input.celular.valor = "";

      $('#modalCombos').modal('toggle');
      $('#verificacionCodigo').addClass('d-none');
      this.respuesta = false;
    },
    enviarNumero: function () {
      this.respuesta = true;
      document.getElementById("exitoCombos").innerHTML = '<div class="row justify-content-center align-self-center" style="height:400px">    <div class="col-1 align-self-center pb-5"><div class="cargando" > <div></div> <div></div> <div></div> <div></div> </div> </div></div>';

      axios.post('https://www2.movistar.com.ec/send-obo-site-new/combo/sms', {
        celular: this.input.celular.valor,
      })
        .then(response => {
          if (response.data.status == true) {
            $('#verificacionCodigo').removeClass('d-none');
            document.getElementById("exitoCombos").innerHTML = "";
            this.codeId = response.data.codeId;
          } else {
            document.getElementById("exitoCombos").innerHTML = response.data.menssage;
            setTimeout(() => {
              $('#modalCombos').modal('hide')
              document.getElementById("exitoCombos").innerHTML = "";
              this.respuesta = false;
            }, 4000);
          }
        })
        .catch(function (error) {
          document.getElementById("exitoCombos").innerHTML = '<div class="row justify-content-center align-self-center" style="height:400px">    <div class="col-1 align-self-center pb-5"><div class="cargando" > <div></div> <div></div> <div></div> <div></div> </div> </div></div>';
          setTimeout(() => {

            $('#modalCombos').modal('hide')
            document.getElementById("exitoCombos").innerHTML = "";
            this.respuesta = false;
          }, 4000);
        });

    },
    verificacionCodigo: function () {
      document.getElementById("exitoCombos").innerHTML = '<div class="row justify-content-center align-self-center" style="height:400px">    <div class="col-1 align-self-center pb-5"><div class="cargando" > <div></div> <div></div> <div></div> <div></div> </div> </div></div>';
      $('#verificacionCodigo').addClass('d-none');
      axios.post('https://www2.movistar.com.ec/send-obo-site-new/combo/verificar', {
        codigoid: this.codeId,
        codigo: this.input.codigo.valor,
        slug: this.planSeleccionado.slug


      }).then(response => {
        debugger;
        console.log('respuestaaaa', response)
        if (response.data.status == true) {
          document.getElementById("exitoCombos").innerHTML = response.data.message;
          $('#codigo').removeClass('error');
        } else {
          $('#verificacionCodigo').removeClass('d-none');
          document.getElementById("exitoCombos").innerHTML = '';
          $('#codigo').addClass('error');
          this.input.codigo.errorServicio = true;
          this.input.codigo.valor = "";
        }
      }).catch(error => console.log('error error', error));
    },
    enviarFormulario: function () {
      this.respuesta = true;
      document.getElementById("exito").innerHTML = '<div class="row " style="height:400px">    <div class="col-12 align-self-center pb-5"><div class="cargando" > <div></div> <div></div> <div></div> <div></div> </div> </div></div>';

      axios.post(' https://www2.movistar.com.ec/send-obo-site-new/send/datos', {

        cellphone: this.input.celular.valor,
        identification: "099999",
        campana: "planes_menu_pruebas",
        producto: this.planSeleccionado.slug,
      })
        .then(response => {
          document.getElementById("exito").innerHTML = response.data.smsObo;

          setTimeout(() => {
            this.respuesta = false;
            $('#exampleModalCenter').modal('hide')

            document.getElementById("exito").innerHTML = "";
          }, 4000);

        })
        .catch((error) => {
          document.getElementById("exito").innerHTML = '<div class="row justify-content-center"><div class="col-md-4 text-center"><span class="font-size-250 icon-Fail"></span><p class="text-magenta-movistar">Upss ocurrio un error</p><p></p></div></div>';
          setTimeout(() => {
            this.respuesta = false;
            $('#exampleModalCenter').modal('hide')

            document.getElementById("exito").innerHTML = "";
          }, 4000);

        });
    },

    handleResize(datosPlanes) {
      if (window.innerWidth <= 768) {
        datosPlanes = this.ordenar(datosPlanes, 'order_mobile');
      } else {
        datosPlanes = this.ordenar(datosPlanes, 'order_desktop');
      }
      return datosPlanes;
    }


  },
})


var carouselEquipos = $('.carousel');

// Bootstrap carousel needs to be loaded first
carouselEquipos.carousel
({
  interval: false,
  wrap:true,
  pause:"hover",
  visible: 2,
}
).swipeCarousel({
  sensitivity: 'high' // low, medium or high
});
$('.carousel').on('slide.bs.carousel', function (slide) {
/* if(slide.direction=='left'){
  $("#imagenesDerecha"+slide.from).fadeOut();
  console.log(slide, "que soy ")

 setTimeout(function() {
  $("#imagenesDerecha"+slide.from).show();
}, 100);
 


}
  console.log(slide);
   */
})

