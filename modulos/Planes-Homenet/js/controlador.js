$('#cambioTexto').on('click', function () {

    var texto = $('#cambioTexto')[0].innerText;

    if (texto == 'Más detalle') {
      $('#cambioTexto')[0].innerText = "Menos detalle";
      $('#cambioSpan').addClass("transform-180");
    } else if (texto == "Menos detalle") {
      $('#cambioTexto')[0].innerText = "Más detalle";
      $('#cambioSpan').removeClass("transform-180");
    }
  });