Vue.filter("entero", function (valor) {
    if (!valor) { return ""; }
    return Math.trunc(valor);
});
Vue.filter("decimal", function (valor) {
    if (!valor) { return ""; }
    var parteDecimal = valor.toFixed(2).toString().split('.')[1];
    return "." + parteDecimal;
});
Vue.filter("redondear", function (valor) {
    if (!valor) { return ""; }   
    return Math.round(valor * 100) / 100;
});