
var appVue = new Vue({
  el: '#contenedor_vue',

  data: {
    accion: "",
    /* DIRECTIVAS */
    input: {
      celular: { valor: "", error: true, muestraMensaje: false, mensaje: '' },
      cedula: { valor: "", error: null, muestraMensaje: false, mensaje: '' } // falta la implementación para la validación de la cédula
    },
    respuesta: false,
    datosCombos: [],
    datosBonos: [],
    precioSeleccionado: {},
    gigasSeleccionado: {},
    estilosPlanDestacado: {
      0: {
        "destacadoBorde": "borde-top-azul-5",
        "destacadoBoton": "btn-azul-movistar",
        "destacadoIcono": "pt-4",
      },
      1: {
        "destacadoBorde": "borde-top-verde-5",
        "destacadoBoton": "btn-verde-movistar",
        "destacadoIcono": "icon-icono-recarga-movil-filled icon-verde-movistar",

      }
    },

    planSeleccionado: "",
  },
  computed: {
    abrir: function () {
      if (this.input.celular.valor.split('').length < 10) {
        return true
      } else if (this.input.celular.error == true) {
        return true
      }

    },
  },


  created() {

    window.addEventListener('resize', this.ejecutarEvento)
    this.cargarData();

  },
  destroyed() {
    window.removeEventListener('resize', this.ejecutarEvento)
  },

  methods: {
    ejecutarEvento: function () {
      this.datosCombos = this.handleResize(this.datosCombos);
      this.datosBonos = this.handleResize(this.datosBonos);
    },

    cargarData: function () {
      let identificadorCatalogo = "catalogo-planes";
      /* axios.post("json/plans_largue.json", {
        identificador: identificadorCatalogo
      }) */
      axios.get("json/plans_largue.json")
        .then(respuesta => {
          this.datosCombos = respuesta.data[identificadorCatalogo].combos;
          this.datosBonos = respuesta.data[identificadorCatalogo].bonos;



          for (const key in this.datosCombos) {
            if (this.datosCombos.hasOwnProperty(key)) {
              const element = this.datosCombos[key];
            }
          }

          /*  this.datosCombos = dummy.planes; */
          this.datosCombos = this.handleResize(this.datosCombos);
          this.datosBonos = this.handleResize(this.datosBonos);

        });

    },
    ordenar: function (datos, key) {
      datos = Object.values(datos);
      datos.sort(function (a, b) {
        return a[key] - b[key];
      });
      return datos;
    },
    cambiarTexto: function (id, texto) {
 
      var textoObtenido = $('#' + texto).text();
      textoObtenido = textoObtenido.replace("\n", "").replace(/ /g, "");
      if (textoObtenido == "Másdetalle") {
        $('#' + texto).text("Menos detalle");
        $('#' + id).addClass("transform-180");
      } else if (textoObtenido == "Menosdetalle") {
        $('#' + texto).text("Más detalle");
        $('#' + id).removeClass("transform-180");
      }
    },
    abrirModalCombos: function (slug, objeto) {
      if (objeto == "bono") {
        this.planSeleccionado = this.datosBonos.find(variable => variable.slug == slug);
      } else if(objeto == "combo") {
        this.planSeleccionado = this.datosCombos.find(variable => variable.slug == slug);
      }

      this.gigasSeleccionado = this.planSeleccionado.benefits.normal_benefits[0];
      this.precioSeleccionado = this.planSeleccionado.prices.price;

      this.input.celular.valor = "";

      $('#modalCombos').modal('toggle');
    },
    enviarNumero: function () {
      this.respuesta = true;
      document.getElementById("exitoCombos").innerHTML = '<div class="row " style="height:400px">    <div class="col-12 align-self-center pb-5"><div class="cargando" > <div></div> <div></div> <div></div> <div></div> </div> </div></div>';

      axios.post('https://www2.movistar.com.ec/send-obo-site-new/combo/sms', {

        celular: this.input.celular.valor,

      })
        .then(response => {
          document.getElementById("exitoCombos").innerHTML = response.data.menssage;
          setTimeout(() => {

            $('#modalCombos').modal('hide')

            document.getElementById("exitoCombos").innerHTML = "";
            this.respuesta = false;
          }, 4000);
        })
        .catch(function (error) {
          document.getElementById("exitoCombos").innerHTML = '<div class="row justify-content-center"><div class="col-md-4 text-center"><span class="font-size-250 icon-Fail"></span><p class="text-magenta-movistar">Upss ocurrio un error</p><p></p></div></div>';
          setTimeout(() => {

            $('#modalCombos').modal('hide')

            document.getElementById("exitoCombos").innerHTML = "";
            this.respuesta = false;
          }, 4000);



        });

    },
    handleResize(datosCombos) {
      if (window.innerWidth <= 768) {
        datosCombos = this.ordenar(datosCombos, 'order_mobile');
      } else {
        datosCombos = this.ordenar(datosCombos, 'order_desktop');
      }
      return datosCombos;
    }


  },
})


$('.carousel').carousel();



