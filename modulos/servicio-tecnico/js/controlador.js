
var appVue = window.appVue = new Vue({
    el: '#contenedor_vue',
    data: {
        provincias: [],
        provinciaSeleccionada: "",
        ciudades: [],
        cavs: [],
        mapa: null,
        marcador: null,
        geoCoder: null,
        autocompletar: null,
        direccionesEncontradas: null,
        formulario: false
    },
    methods: {
        consumir: function () {
            axios.get('https://www2.movistar.com.ec/medusa/tentaculo/file/json/DB-servicio-tecnico-locales')
                .then(response => (
                    this.provincias = response.data.provincias
                ))
                .catch(error => (
                    console.log('NO', error)
                ));

        },
        onChangeProvincia: function (event) {
            this.cavs = [];
            console.log(event.target.value)
            this.provinciaSeleccionada = event.target.value;
            if (event.target.value != "") {
                let list = [];
                $.each(this.provincias, function (key, value) {
                    // console.log(key, event.target.value);
                    if (key == event.target.value) {
                        // console.log(key, value);
                        list.push(value);
                    }
                });
                // console.log('list', list[0]);
                this.ciudades = list[0];
                let letras = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

                $.each(this.ciudades, function (key, value) {
                    let aux = 0;
                    $.each(value, function (k, v) {
                        v.letra = letras[aux];
                        aux++;
                    });
                });
            }
        },
        onChangeCiudad: function (event) {
            this.cavs = [];
            let letras = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
            console.log(event.target.value)
            if (event.target.value != "") {
                let list = [];
                $.each(this.ciudades, function (key, value) {
                    if (key == event.target.value) {
                        list.push(value);
                    }
                });
                // console.log('listC', list[0]);
                this.cavs = list[0];
                let aux = 0;
                $.each(this.cavs, function (k, v) {
                    v.letra = letras[aux];
                    aux++;
                });

                // this.createMap();
                this.mostrarUbicacionPorLatLng();
            }
        },
        cambiarTexto: function (id, texto) {
            // console.log(id, texto);
            var textoObtenido = $('#' + texto).text()
            textoObtenido = textoObtenido.replace("\n", "");
            textoObtenido = textoObtenido.replace(/ /g, "");

            if (textoObtenido == "Vermás") {
                $('#' + texto).text("Ver menos")
                $('#' + id).addClass("transform-180");
            } else if (textoObtenido == "Vermenos") {
                $('#' + texto).text("Ver más")
                $('#' + id).removeClass("transform-180");
            }
        },
        mapaMovile: function (id, texto, local, val, ciudad) {
            // console.log(id, texto, local.FormularioIngreso);
            var textoObtenido = $('#' + texto).text()
            textoObtenido = textoObtenido.replace("\n", "");
            textoObtenido = textoObtenido.replace(/ /g, "");

            if (textoObtenido == "Vermás") {

                $('#' + texto).text("Ver menos")
                $('#' + id).addClass("transform-180");
                if (local.FormularioIngreso == true) {
                    $('#formulario-' + val + ciudad).removeClass("d-none");
                }
            } else if (textoObtenido == "Vermenos") {
                if (local.FormularioIngreso == true) {
                    $('#formulario-' + val + ciudad).addClass("d-none");
                }
                $('#' + texto).text("Ver más")
                $('#' + id).removeClass("transform-180");
            }

            this.geoCoder = new google.maps.Geocoder();
            this.geoCoder.geocode({ 'address': local.direccion }, function (results, status) {

                if (status == google.maps.GeocoderStatus.OK) {
                    local.location = results[0].geometry.location;
                    var latitud = results[0].geometry.location.lat();
                    var longitud = results[0].geometry.location.lng();

                    var mapa = new google.maps.Map(document.getElementById('map-' + val + ciudad), {
                        center: { lat: latitud, lng: longitud },
                        zoom: 16
                    });
                    var infoWindow = new google.maps.InfoWindow();

                    this.marcador = new google.maps.Marker({
                        position: { lat: latitud, lng: longitud },
                        map: mapa
                    });

                    google.maps.event.addListener(this.marcador, 'click', function () {
                        var markerContent = '<h5 class="mb-0 text-gris-movistar-5 text-left ml-4">' +
                            local.nombre + '</h5><p class="text-left">' + local.direccion + '</p>';
                        infoWindow.setContent(markerContent);
                        infoWindow.open(mapa, this);
                    });
                }

            });
        },
        activeCav: function (id, ubicacion, nombre, direccion, letra) {
            // console.log('id', id, ubicacion);
            $.each(this.cavs, function (key, value) {
                // console.log(key, value);
                $('#cav-' + key).removeClass('cavs-box-active');
            });
            $('#' + id).addClass('cavs-box-active');

            this.mapa = new google.maps.Map(document.getElementById('map'), {
                center: { lat: ubicacion.lat(), lng: ubicacion.lng() },
                zoom: 16
            });

            var infoWindow = new google.maps.InfoWindow();

            this.marcador = new google.maps.Marker({
                position: { lat: ubicacion.lat(), lng: ubicacion.lng() },
                map: this.mapa
            });

            google.maps.event.addListener(this.marcador, 'click', function () {
                var markerContent = '<h5 class="mb-0 text-gris-movistar-5 text-left ml-4">' +
                    '<span class="letra-cav">' + letra + '</span> &nbsp;' +
                    nombre + '</h5><p class="text-left">' + direccion + '</p>';
                infoWindow.setContent(markerContent);
                infoWindow.open(this.mapa, this);
            });

            // console.log('M', this.marcador);
            // this.marcador.setMap(this.mapa);
        },

        createMap: function () {
            this.mapa = new google.maps.Map(document.getElementById('map'), {
                center: { lat: -0.2144696, lng: -78.4968223 },
                zoom: 16
            });
        },
        mostrarUbicacionPorLatLng: function () {

            let marcadores = [];
            var aux = 0;

            $.each(this.cavs, function (k, v) {
                // console.log('MAR', k, v.direccion);
                this.geoCoder = new google.maps.Geocoder();
                this.geoCoder.geocode({ 'address': v.direccion }, function (results, status) {

                    if (status == google.maps.GeocoderStatus.OK) {
                        // console.log('LC', results[0].geometry.location.lat());
                        v.location = results[0].geometry.location;
                        var latitud = results[0].geometry.location.lat();
                        var longitud = results[0].geometry.location.lng();

                        this.mapa = new google.maps.Map(document.getElementById('map'), {
                            center: { lat: latitud, lng: longitud },
                            zoom: 16
                        });
                        var infoWindow = new google.maps.InfoWindow();

                        marcadores[aux] = new google.maps.Marker({
                            position: { lat: latitud, lng: longitud },
                            map: this.mapa
                        });

                        google.maps.event.addListener(marcadores[aux], 'click', function () {
                            var markerContent = '<h5 class="mb-0 text-gris-movistar-5 text-left ml-4">' +
                                '<span class="letra-cav">' + v.letra + '</span>&nbsp' +
                                v.nombre + '</h5><p class="text-left">' + v.direccion + '</p>';
                            infoWindow.setContent(markerContent);
                            infoWindow.open(this.map, this);
                        });
                        // console.log('M', marcadores);
                        // marcadores.setMap(this.mapa);
                        aux++;
                    }

                });
            });

            $.each(marcadores, function (k, v) {
                console.log(k, v);
            });
        },
        colocarMarcadores: function (latitud, longitud) {

        }
    },
    mounted: function () {
        this.consumir();
        setTimeout(this.createMap, 100);
    },
})

