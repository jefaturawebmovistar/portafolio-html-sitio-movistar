Vue.directive('focus', {

    // Cuando el elemento enlazado es insertado en el DOM

    inserted: function (el) {
        console.log("llegué aquí");
        // Pon el foco en el elemento
        el.focus();
        console.log("llegué aquí también");
    }
})
var appVue = new Vue({
    el: '#contenedor_vue',

    data: {
        datosModal: "",
        tipoAccesibilidad: {
            visionNula: '<div class="d-flex">\
            <img src="imagenes/vision-nula.svg" class="rounded" width="20%">\
            <p class="subtitle3 bold py-4 mb-0 pl-4">Visión Nula</p>\
          </div>\
          <div class="py-3">\
            <p class="bold mb-0 caption">Tipos de lector de pantalla:</p>\
            <p class="caption">VoiceOver (Apple) - viene preinstalado tanto en el iPhone como en el iPad, iPod y el\
              iWatch.\
              <br>TalkBack (Android) - no siempre viene preinstalado, puede descargarse gratis del Play Store.\
              <br>Narrator (Windows Phone) - preinstalado a partir de la versión 10 y en algunos casos con la segunda\
              actualización de la versión 8.1- (Consulte el análisis del Narrator de Windows Phone).\
              <br>Se recomienda tener actualizado el lector de pantalla y las voces a la última versión al igual que\
              probar los dispositivos en la tienda antes de adquirirlo.\
            </p>',
            audicionNula: '<div class="d-flex">\
            <img src="imagenes/audicion-nula.svg" width="10%">\
            <p class="subtitle3 bold py-4 mb-0 pl-4">Audición Nula</p>\
          </div>\
          <div class="py-3">\
            <p class="bold mb-0 caption">Tipos de lector de pantalla:</p>\
            <p class="caption">VoiceOver (Apple) - viene preinstalado tanto en el iPhone como en el iPad, iPod y el\
              iWatch.\
              <br>TalkBack (Android) - no siempre viene preinstalado, puede descargarse gratis del Play Store.\
              <br>Narrator (Windows Phone) - preinstalado a partir de la versión 10 y en algunos casos con la segunda\
              actualización de la versión 8.1- (Consulte el análisis del Narrator de Windows Phone).\
              <br>Se recomienda tener actualizado el lector de pantalla y las voces a la última versión al igual que\
              probar los dispositivos en la tienda antes de adquirirlo.\
            </p>',
            accesibilidadInfo: '<div class="d-flex">\
            <img src="imagenes/accesibilidad.svg" width="10%">\
            <p class="subtitle3 bold py-4 mb-0 pl-4">Accesibilidad</p>\
          </div>\
          <div class="py-3">\
            <p class="bold mb-0 caption">Tipos de lector de pantalla:</p>\
            <p class="caption">VoiceOver (Apple) - viene preinstalado tanto en el iPhone como en el iPad, iPod y el\
              iWatch.\
              <br>TalkBack (Android) - no siempre viene preinstalado, puede descargarse gratis del Play Store.\
              <br>Narrator (Windows Phone) - preinstalado a partir de la versión 10 y en algunos casos con la segunda\
              actualización de la versión 8.1- (Consulte el análisis del Narrator de Windows Phone).\
              <br>Se recomienda tener actualizado el lector de pantalla y las voces a la última versión al igual que\
              probar los dispositivos en la tienda antes de adquirirlo.\
            </p>',
            manipulacionLimitada: '<div class="d-flex">\
            <img src="imagenes/manipulacion-limitada.svg" width="10%">\
            <p class="subtitle3 bold py-4 mb-0 pl-4">Manipulación limitada</p>\
          </div>\
          <div class="py-3">\
            <p class="bold mb-0 caption">Tipos de lector de pantalla:</p>\
            <p class="caption">VoiceOver (Apple) - viene preinstalado tanto en el iPhone como en el iPad, iPod y el\
              iWatch.\
              <br>TalkBack (Android) - no siempre viene preinstalado, puede descargarse gratis del Play Store.\
              <br>Narrator (Windows Phone) - preinstalado a partir de la versión 10 y en algunos casos con la segunda\
              actualización de la versión 8.1- (Consulte el análisis del Narrator de Windows Phone).\
              <br>Se recomienda tener actualizado el lector de pantalla y las voces a la última versión al igual que\
              probar los dispositivos en la tienda antes de adquirirlo.\
            </p>',
            comprensionInfo: '<div class="d-flex">\
            <img src="imagenes/comprension.svg" width="10%">\
            <p class="subtitle3 bold py-4 mb-0 pl-4">Comprensión</p>\
          </div>\
          <div class="py-3">\
            <p class="bold mb-0 caption">Tipos de lector de pantalla:</p>\
            <p class="caption">VoiceOver (Apple) - viene preinstalado tanto en el iPhone como en el iPad, iPod y el\
              iWatch.\
              <br>TalkBack (Android) - no siempre viene preinstalado, puede descargarse gratis del Play Store.\
              <br>Narrator (Windows Phone) - preinstalado a partir de la versión 10 y en algunos casos con la segunda\
              actualización de la versión 8.1- (Consulte el análisis del Narrator de Windows Phone).\
              <br>Se recomienda tener actualizado el lector de pantalla y las voces a la última versión al igual que\
              probar los dispositivos en la tienda antes de adquirirlo.\
            </p>'
        }


    },
    methods: {
        abrirModalAccesibilidad: function (data) {
            console.log('ACC', data);
            $('#modalAccesibilidad').modal('toggle');
            this.datosModal = this.tipoAccesibilidad[data];
            console.log('datosModal', this.datosModal);
        },
        closeModal: function(){
          console.log('HIDE');
          // $('#modalAccesibilidad').modal('hide');
        }
    },
    mounted: function () {
    },
})


